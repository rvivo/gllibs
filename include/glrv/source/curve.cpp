#include <iostream>
#include <glrv/include/curve.h>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>

// CURVA ---------------------------------------------------------------------------
void Curva::dibujar()                      //Dibujo inmediato sin DL
{
   glBegin(GL_LINE_STRIP);
    for (int i=0; i<101; i++){
		glVertex4fv(glm::value_ptr(q(i*0.01f)));
    };
    glEnd();
    glBegin(GL_LINES);
    for (float u=0; u<=1; u+=0.01){
		Vector pos=q(u).xyz;			//cuidado con el swizzle. Devuelve referencias no objetos del tipo
		Vector incn=n(u)*0.1f;
		Vector incb=b(u)*0.1f;
        glVertex3fv(glm::value_ptr( pos ));
		glVertex3fv(glm::value_ptr( pos + incn ));
        glVertex3fv(glm::value_ptr( pos ));
		glVertex3fv(glm::value_ptr( pos + incb ));
    };
    glEnd();
}

// HELICE -------------------------------------------------------------------------
Punto  Helice::q(float u) const                     //Muestreo de la curva en u
{
    Punto p(sin(u*dPI)*radio, u*paso, cos(u*dPI)*radio, 1.0);
    return p;
}
Vector Helice::t(float u) const                     //Vector tangente unitario a la curva en u
{
	Vector tg(cos(u*dPI)*radio*dPI, paso, -sin(u*dPI)*radio*dPI);
	return glm::normalize(tg);
}
Vector Helice::b(float u) const                     //Vector binormal unitario en u
{
	Vector a(-sin(u*dPI),0.0,-cos(u*dPI));
	Vector bn(glm::cross(t(u),a));
	return glm::normalize(bn);
}
Vector Helice::n(float u) const                     //Vector normal unitario en u
{
    Vector a(-sin(u*dPI),0.0,-cos(u*dPI));
	Vector normal(glm::cross(t(u),glm::cross(t(u),a)));
	return glm::normalize(normal);
}

// CURVA BEZIER -----------------------------------------------------------------
const glm::mat4 CBezier::MB(glm::vec4(-1.f, 3.f,-3.f, 1.f),      //Matriz característica de Bezier
							glm::vec4( 3.f,-6.f, 3.f, 0.f),
							glm::vec4(-3.f, 3.f, 0.f, 0.f),
							glm::vec4( 1.f, 0.f, 0.f, 0.f));

CBezier::CBezier(Punto const control[4])                  //Construye con P0..P3
{
	G=glm::transpose(glm::mat4(control[0],control[1],control[2],control[3]));
	C=MB*G;
}
Punto  CBezier::q(float u) const                    //Q(u) por muestreo directo
{
    float uu=u*u, uuu=uu*u;
	Punto U(uuu,uu,u,1.0);
    return U*C;
}
Vector CBezier::t(float u) const                    //Vector tangente unitario en u
{
	Punto Uprima(3*u*u,2*u,1.0,0.0);
	return glm::normalize(Vector(Uprima*C));
}
Vector CBezier::b(float u) const                    //Vector binormal unitario en u
{
    static Vector bna;								//binormal anterior
	Punto Usegunda(6*u,2.0,0.0,0.0);
    Vector a(Usegunda*C);
	Vector bn= glm::cross(t(u),a);					//binormal actual
	if(u==0) bna=bn;								//inicializa la binormal anterior
    if(bn.length()==0) bn=bna;						//Justo en el punto de inflexión
	if(glm::dot(bn,bna)<0) bn= -bn;					//cambia la binormal de sentido
    bna=bn;											//actualiza la binormal actual
	return glm::normalize(bn);
}
Vector CBezier::n(float u) const                    //Vector normal en u
{
	Vector normal= glm::cross(t(u),b(u));
	return glm::normalize(normal);
}

// CURVA KOCHANEK_BARTELS -----------------------------------------------------------
/* Es un de Hermite con las tangentes calculadas
a partir de puntos de control anteriores y posteriores */
static const glm::mat4 MH = glm::transpose(glm::mat4(
	glm::vec4(2.f, -2.f, 1.f, 1.f),      //Matriz característica de HERMITE (por columnas)
	glm::vec4(-3.f, 3.f, -2.f, -1.f),
	glm::vec4(0.f, 0.f, 1.f, 0.f),
	glm::vec4(1.f, 0.f, 0.f, 0.f)));

SplineKB::SplineKB(Punto const control[4], float t, float b, float c)                  //Construye con P0..P3
{
	Punto p0 = control[1];																						// Puntos de paso
	Punto p1 = control[2];
	Punto r0 = (1 - t)*(1 + b)*(1 + c) / 2 * (p0 - control[0]) + (1 - t)*(1 - b)*(1 - c) / 2 * (p1 - p0);		// Tangentes
	Punto r1 = (1 - t)*(1 + b)*(1 - c) / 2 * (p1 - p0) + (1 - t)*(1 - b)*(1 + c) / 2 * (control[3] - p1);

	G = glm::transpose(glm::mat4(p0, p1, r0, r1));
	C = MH*G;
}
Punto  SplineKB::q(float u) const                    //Q(u) por muestreo directo
{
	float uu = u*u, uuu = uu*u;
	Punto U(uuu, uu, u, 1.0);
	return U*C;
}
Vector SplineKB::t(float u) const                    //Vector tangente unitario en u
{
	Punto Uprima(3 * u*u, 2 * u, 1.0, 0.0);
	return glm::normalize(Vector(Uprima*C));
}
Vector SplineKB::b(float u) const                    //Vector binormal unitario en u
{
	static Vector bna;								//binormal anterior
	Punto Usegunda(6 * u, 2.0, 0.0, 0.0);
	Vector a(Usegunda*C);
	Vector bn = glm::cross(t(u), a);					//binormal actual
	if (u == 0) bna = bn;								//inicializa la binormal anterior
	if (bn.length() == 0) bn = bna;						//Justo en el punto de inflexión
	if (glm::dot(bn, bna)<0) bn = -bn;					//cambia la binormal de sentido
	bna = bn;											//actualiza la binormal actual
	return bn;
}
Vector SplineKB::n(float u) const                    //Vector normal en u
{
	Vector normal = glm::cross(t(u), b(u));
	return glm::normalize(normal);
}

/************** CurvePath *************/
/* Curva compuesta de tramos SplineKB */

CurvePath::CurvePath(const std::vector<Punto> trazado, float tension, float bias)
{
	int n = trazado.size();
	_tension = tension;
	_bias = bias;
	if (n<4) {
		std::cerr << "CurvePath::CurvePath: Al menos 4 puntos de control" << std::endl;
		return;
	}
	// N puntos, N-3 tramos en curva abierta, N en cerrada
	// Curva cerrada:
	Punto pControl[] = { trazado[0],trazado[1],trazado[2],trazado[3] };
	for (int i = 0; i < n; i++) {
		controlPoints.push_back(trazado[i]);
		tramos.push_back(new SplineKB(pControl, _tension, _bias));
		pControl[0] = pControl[1];
		pControl[1] = pControl[2];
		pControl[2] = pControl[3];
		pControl[3] = trazado[(i + 4) % n];
	}
}
void CurvePath::addPoint( Punto  p)
{
	controlPoints.push_back(p);
	int k = controlPoints.size();
	if (k < 4) return;
	tramos.clear();
	Punto pControl[] = { controlPoints[0], controlPoints[1], controlPoints[2], controlPoints[3] };
	for (int i = 0; i < k; i++) {
		tramos.push_back(new SplineKB(pControl, _tension, _bias));
		pControl[0] = pControl[1];
		pControl[1] = pControl[2];
		pControl[2] = pControl[3];
		pControl[3] = controlPoints[(i + 4) % k];
	}
}
void CurvePath::movePoint(int index, Punto pos)
{
	int k = controlPoints.size();
	if (index >= k || k<4) return;
	controlPoints[index] = pos;
	tramos.clear();
	Punto pControl[] = { controlPoints[0], controlPoints[1], controlPoints[2], controlPoints[3] };
	for (int i = 0; i < k; i++) {
		tramos.push_back(new SplineKB(pControl, _tension, _bias));
		pControl[0] = pControl[1];
		pControl[1] = pControl[2];
		pControl[2] = pControl[3];
		pControl[3] = controlPoints[(i + 4) % k];
	}
}
Punto  CurvePath::q(float u) const
{
	/* La u va de 0 a 1 en todo el trazado
	   Se debe calcular la u interior en cada tramo
	   Para ello se escala la u por el numero de tramos.
	   La parte entera sera el tramo (empezando en 0) y
	   la decimal la u interior a ese tramo*/

	// Numero de tramos
	int ntramos = tramos.size();
	if (ntramos == 0) return Punto();
	// donde estamos?
	int tramo = int(u*ntramos);

	// la u en el tramo es la parte decimal
	float utramo = u*ntramos - tramo;

	// si la u es 1, el tramo debe ser el primero (0)
	if (tramo == ntramos) tramo = 0;

	return tramos[tramo]->q(utramo);
}
Vector CurvePath::t(float u) const
{
	int ntramos = tramos.size();
	if (ntramos == 0) return Vector();
	int tramo = int(ntramos * u);
	float utramo = u*ntramos - tramo;
	if (u == 1) tramo = 0;
	return tramos[tramo]->t(utramo);
}
Vector CurvePath::b(float u) const
{
	int ntramos = tramos.size();
	if (ntramos == 0) return Vector();
	int tramo = int(ntramos * u);
	float utramo = u*ntramos - tramo;
	if (u == 1) tramo = 0;
	return tramos[tramo]->b(utramo);
}
Vector CurvePath::n(float u) const
{
	int ntramos = tramos.size();
	if (ntramos == 0) return Vector();
	int tramo = int(ntramos * u);
	float utramo = u*ntramos - tramo;
	if (u == 1) tramo = 0;
	return tramos[tramo]->n(utramo);
}
void CurvePath::draw() const
{
	for (int i = 0; i<tramos.size(); i++) {
		tramos[i]->dibujar();
	}
}