// Transform.cpp: implementation of the Transform functions

#include <GL/freeglut.h>
#include <glrv/include/Transform.h>

// Transform namespace ----------------------------------------------------------
// Gets the translation matrix
mat4 Transform::translate(const vec3& translation){
	mat4 T = mat4(1.0);
	T[0][3] = translation.x;
	T[1][3] = translation.y;
	T[2][3] = translation.z;
	return T;
}

// Gets the rotation matrix around an axis. Uses Rodrigues' formulae
mat3 Transform::rotate(const float degrees, const vec3& axis) {
	mat3 I=mat3(1.0);
	mat3 ATA=mat3(axis.x*axis.x,axis.y*axis.x,axis.z*axis.x,	//first column
		          axis.x*axis.y,axis.y*axis.y,axis.z*axis.y,	//second column
				  axis.x*axis.z,axis.y*axis.z,axis.z*axis.z);	//third column
	mat3 Astar=mat3(0.0,axis.z,-axis.y,							//first column
					-axis.z,0.0,axis.x,							//second column
					axis.y,-axis.x,0.0);						//third column
	mat3 rot= cos(degrees*PI/180.0f)*I+(1-cos(degrees*PI/180.0f))*ATA+sin(degrees*PI/180.0f)*Astar;
  // You will change this return call
  return rot;
}

// Transforms the camera left around the "crystal ball" interface
void Transform::left(float degrees, vec3& eye, vec3& up) {
	vec3 u= glm::normalize(glm::cross(eye,up));
	up= glm::normalize(glm::cross(u,eye));
	// cuidado con significado de izq. y derecha
	mat3 rot= rotate(-degrees,glm::normalize(up));
	eye=rot*eye;
}

// Transforms the camera up around the "crystal ball" interface
void Transform::up(float degrees, vec3& eye, vec3& up) {
  // YOUR CODE FOR HW1 HERE 
	vec3 aor= glm::normalize(glm::cross(eye,up));
	mat3 rot= rotate(degrees,aor);
	eye=rot*eye;
	up= glm::normalize(glm::cross(aor,eye));
}

// Implementation of the glm::lookAt matrix
mat4 Transform::lookAt(vec3 eye, vec3 up) {
  // YOUR CODE FOR HW1 HERE
	//Camera's system
	vec3 u= glm::normalize(glm::cross(up,eye));
	vec3 v= glm::normalize(glm::cross(eye,u));
	vec3 w= glm::normalize(eye);
	//Espera la traspuesta, por eso esta por filas
	//Se combina Rn*Tn en una sola matriz as�:
	return mat4(u.x,u.y,u.z,glm::dot(u,-eye),
				v.x,v.y,v.z,glm::dot(v,-eye),
				w.x,w.y,w.z,glm::dot(w,-eye),
				0.0,0.0,0.0,1.0);

}

// Matriz de cambio de base rigida
// RS � p = newRS � p'  ->  p' = to( newRS ) � p;
mat4 Transform::to(const RS& newRS) {
	// Construye column major por eso traspone
	return glm::transpose( mat4(vec4(newRS.uvec, glm::dot(newRS.uvec, -newRS.origin)),
								vec4(newRS.vvec, glm::dot(newRS.vvec, -newRS.origin)),
								vec4(newRS.wvec, glm::dot(newRS.wvec, -newRS.origin)),
								vec4(0.0, 0.0, 0.0, 1.0)
							   )
						 );
}

// Matriz de cambio inverso de base rigida
// RS � p = newRS � p'  ->  p = from(newRS) � p'
mat4 Transform::from(const RS& newRS){
	// Construye por column major, no hace falta trasponer
	mat4 result;
	result = mat4(vec4(newRS.uvec,0.0),
		        vec4(newRS.vvec,0.0),
				vec4(newRS.wvec,0.0),
				vec4(newRS.origin,1.0));
	return result;
}

// Deducir un sistema nuevo, conocidos 3 puntos en ambos sistemas y el origen del nuevo en el padre
// Es un sistema de ecuaciones 3x3
// [ux uy uz]        [p0u p0v p0w]     [p0 - origen]
// [vx vy vz] = inv( [p1u p1v p1w] ) * [p1 - origen]
// [wx wy wz]        [p2u p2v p2w]     [p2 - origen]
RS* Transform::newRS(const RS& parent, const vec3& origin, const mat3& p, const mat3& newp) {
	// @parent: sistema al que se refiere el nuevo buescado
	// @origin: coordenadas del origen del nuevo sistema en el sistema padre
	// @p: 3 puntos en columna en el sistema padre
	// @newp: mismos 3 puntos en columna referidos al nuevo sistema
	// Los puntos vienen en columna, es decir, se definieron mat3(p0,p1,p2)
	mat3 G = glm::inverse(glm::transpose(newp));
	mat3 o = glm::mat3(origin, origin, origin);
	mat3 T = glm::transpose(p - o);
	mat3 BASE = glm::transpose(G * T);
	RS* hijo = new RS(
		origin,
		BASE[0],	//u 1a. columna
		BASE[1],	//v 2a. columna
		BASE[2]);	//w 3a. columna
	hijo->setParent((RS*) &parent);
	return hijo;
}
	
// RS class ----------------------------------------------------------------------

// Devuelve la modelView (transformaciones respecto a su padre) de ese sistema
mat4 RS::localMatrix()const{
	return Transform::from(*this);
}

// Devuelve la concatenaci�n de modelView's hasta la r�iz
mat4 RS::worldMatrix()const{
	mat4 wm = localMatrix();
	RS* p = parent;
	while (p != nullptr){
		wm = p->localMatrix() * wm;
		p = p->getParent();
	};
	return wm;
}

// Aplica una translacion al origen del sistema
void RS::translate(const vec3& translation){
	origin = origin + translation;
}

// Aplica un giro al sistema alredor de un eje
void RS::rotate(const float degrees, const vec3& axis){
	mat3 R = Transform::rotate(degrees, axis);
	origin = R*origin;
	uvec = R*uvec;
	vvec = R*vvec;
	wvec = R*wvec;
}

// Dibuja unos ejes de longitud l
void RS::draw(const mat4& viewMatrix, float l)const{
	// V�rtices en el sistema de referencia del modelo
	vec4 p0(0.0, 0.0, 0.0, 1.0);
	vec4 p1(l, 0.0, 0.0, 1.0);
	vec4 p2(0.0, l, 0.0, 1.0);
	vec4 p3(0.0, 0.0, l, 1.0);

	mat4 modelView = viewMatrix * worldMatrix();
	p0 = modelView * p0;
	p1 = modelView * p1;
	p2 = modelView * p2;
	p3 = modelView * p3;

	// Dibujar en el sistema de la vista
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(p0.x, p0.y, p0.z);
	glVertex3f(p1.x, p1.y, p1.z);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(p0.x, p0.y, p0.z);
	glVertex3f(p2.x, p2.y, p2.z);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(p0.x, p0.y, p0.z);
	glVertex3f(p3.x, p3.y, p3.z);
	glEnd();
}

// Debug ----------------------------------------------------------------------
void print(const vec3& v){
	std::cout << "vector(" << v.x << "," << v.y << "," << v.z << ")" << std::endl;
}

void print(const mat4& m){
	std::cout << "[";
	for (int i = 0; i < 4; i++){
		std::cout << m[i][0] << ", " << m[i][1] << ", " << m[i][2] << ", " << m[i][3] << std::endl;
	}
	std::cout << "]" << std::endl;
}

