#include <glrv/include/axis.h>
void Ejes::construir()
{
	/** En algunas tarjetas da error o no funciona el delegar el id de la lista **/
	/** Dejo la construcción en retenido, pero dibujo en inmediato **/
    //Construye la Display List compilada de una flecha vertical
    id = glGenLists(1);					//Obtiene un id's para las DLs
	GLuint aux = glGenLists(1);
    glNewList(aux,GL_COMPILE);			//Abre la DL
        //Brazo de la flecha
        glBegin(GL_LINES);
            glVertex3f(0,0,0);
            glVertex3f(0,1,0);
        glEnd();
        //Punta de la flecha
        glPushMatrix();
        glTranslatef(0,1,0);
        glRotatef(-90,1,0,0);
        glTranslatef(0.0,0.0,-1/10.0);
        //GLUquadric* punta= gluNewQuadric();
        //gluCylinder(punta,1/50.0,0,1/10.0,10,1);
		glutSolidCone(1/50.0,1/10.0,10,1);
        glPopMatrix();
    glEndList();						//Cierra la DL

    //Ahora construye los ejes
    glNewList(id,GL_COMPILE);
		glPushAttrib(GL_ENABLE_BIT|GL_CURRENT_BIT);
		glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
        //Eje X en rojo
        glColor3f(1,0,0);
        glPushMatrix();
            glRotatef(-90,0,0,1);
            glCallList(aux);
        glPopMatrix();
        //Eje Y en verde
        glColor3f(0,1,0);
        glPushMatrix();
            glCallList(aux);
        glPopMatrix();
        //Eje Z en azul
        glColor3f(0,0,1);
        glPushMatrix();
            glRotatef(90,1,0,0);
            glCallList(aux);
        glPopMatrix();
        //Esferita en el origen
        if(esfera){
            glColor3f(0.5,0.5,0.5);
            //GLUquadric* bolita= gluNewQuadric();
            //gluSphere(bolita,0.05,8,8);
			glutSolidSphere(0.05,8,8);
        };
		glPopAttrib();
    glEndList();
}
void Ejes::dibujar(){
//Dibuja inmediato
    GLuint flecha = glGenLists(1);					//Obtiene un id's para las DLs
    glNewList(flecha,GL_COMPILE);					//Abre la DL
        //Brazo de la flecha
        glBegin(GL_LINES);
            glVertex3f(0,0,0);
            glVertex3f(0,1,0);
        glEnd();
        //Punta de la flecha
        glPushMatrix();
        glTranslatef(0,1,0);
        glRotatef(-90,1,0,0);
        glTranslatef(0.0,0.0,-1/10.0);
        //GLUquadric* punta= gluNewQuadric();
        //gluCylinder(punta,1/50.0,0,1/10.0,10,1);
		glutSolidCone(1/50.0,1/10.0,10,1);
        glPopMatrix();
    glEndList();						//Cierra la DL

    //Ahora construye los ejes
	glPushAttrib(GL_ENABLE_BIT|GL_CURRENT_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
    //Eje X en rojo
    glColor3f(1,0,0);
    glPushMatrix();
        glRotatef(-90,0,0,1);
        glCallList(flecha);
    glPopMatrix();
    //Eje Y en verde
    glColor3f(0,1,0);
    glPushMatrix();
    glCallList(flecha);
    glPopMatrix();
    //Eje Z en azul
    glColor3f(0,0,1);
    glPushMatrix();
        glRotatef(90,1,0,0);
        glCallList(flecha);
    glPopMatrix();
    //Esferita en el origen
    if(esfera){
        glColor3f(0.5,0.5,0.5);
        //GLUquadric* bolita= gluNewQuadric();
        //gluSphere(bolita,0.05,8,8);
		glutSolidSphere(0.05,8,8);
    };
	glPopAttrib();
    glDeleteLists(flecha,1);
}
