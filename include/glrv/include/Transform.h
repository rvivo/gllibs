/*
**	Recursos para manejo de transformaciones sobre sistemas de referencia
**	@AUTHOR rvivo / rvivo@upv.es
**  @DATE	2016
**  @REVISION 0.0	Functions as static class funcs
**  @REVISION 0.1	Changed class by namespace
**	@REVISION 0.2   Added 
**  Examples of use
1)
RS modelRS = RS( position, x', y', z' );
RS viewRS  = RS( eye, u, v, w );
const vec4 modelCoord( 10.0, 9.0, -3.2, 1.0 );

vec4 worldCoord = Transform::from( modelRS ) * modelCoord;
vec4 viewCoord  = Transform::to( viewRS ) * worldCoord;
draw viewCoord;

2)
RS main, model, submodel;
RS view  = RS( eye, u, v, w );
submodel.setParent(model);
model.setParent(main);

model.rotate(45,mainAxis);			// model.localMatrix = R(45,mainAxis);
model.translate(mainDirection);     // model.localMatrix = T(mainDirection) * R(45,mainAxis)

submodel.translate(modelDirection); // subModel.localMatrix = T(modelDirection)

mat viewMatrix = Transform::to( view );

main.draw(view);
model.draw(view);
submodel.draw(view);

*/

#ifndef RV_TRANSFORMS_H	
#define RV_TRANSFORMS_H 0.1

#include <iostream>
#include <glrv/include/definitions.h>
#include <glm/gtc/matrix_transform.hpp>

typedef glm::mat3 mat3 ;
typedef glm::mat4 mat4 ; 
typedef glm::vec3 vec3 ; 
typedef glm::vec4 vec4 ; 


class RS
{
	RS* parent;
public:
	// SR al que se refieren las coordenadas de �ste
	// @TODO: Enlazar SRs
	vec3	origin;
	vec3	uvec;
	vec3	vvec;
	vec3	wvec;
	RS( vec3 o = vec3(0.0, 0.0, 0.0),
		vec3 u = vec3(1.0, 0.0, 0.0),
		vec3 v = vec3(0.0, 1.0, 0.0),
		vec3 w = vec3(0.0, 0.0, 1.0)) :  origin(o), uvec(u), vvec(v), wvec(w), parent(nullptr) {}
	void reset(){
		origin = vec3(0.0, 0.0, 0.0);
		uvec = vec3(1.0, 0.0, 0.0);
		vvec = vec3(0.0, 1.0, 0.0);
		wvec = vec3(0.0, 0.0, 1.0);
	}
	void setParent(RS* p){ parent = p; }
	RS* getParent()const{ return parent; }
	mat4 localMatrix()const;
	mat4 worldMatrix()const;
	void translate(const vec3& translation);
	void rotate(const float degrees, const vec3& axis);
	void draw(const mat4& viewMatrix, float l=1.0f)const;
};

namespace Transform  
{
	// Mueve la camara hacia la izquierda sobre una esfera centrada en POI
	void left(float degrees, vec3& eye, vec3& up);

	// Mueve la camara hacia arriba sobre una esfera centrada en POI
	void up(float degrees, vec3& eye, vec3& up);

	// Devuelve la viewmatrix. @eye: POV-POI; @up: vertical subjetiva
	mat4 lookAt(vec3 eye, vec3 up);

	// Devuelve la matriz de rotacion 3x3. Usa Rodrigues.
	mat3 rotate(const float degrees, const vec3& axis) ;

	// Devuelve la matriz de translacion
	mat4 translate(const vec3& translation);

	// Devuelve la matriz que transforma las coordenadas al nuevo sistema de referencia
	mat4 to(const RS& newRS);

	// Devuelve la matriz que transforma coordenadas del nuevo sistema al principal
	mat4 from(const RS& newRS);

	// Devuelve el sistema de referencia conocidos 3 puntos y el origen (ver apuntes)
	// Los puntos en columna dentro de las matrices, por favor
	RS* newRS(const RS& parent, const vec3& origin, const mat3& p, const mat3& newp);
};

// Utilidades para depuracion
void print(const vec3& v);
void print(const mat4& m);

#endif