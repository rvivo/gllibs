/***************************************************
Ejes
Roberto Viv�, 2012 (v1.0)
Roberto Viv�, 2013 (v1.1) Uso de displaylist en vez de inmediato

Construye unos ejes en el origen de longitud 1 con una
esfera opcional en el origen. Usa displaylists para dibujar.

Dependencias:
glut.h
***************************************************/
#ifndef RV_AXIS_H
#define RV_AXIS_H

#include <GL/freeglut.h>

// EJES -------------------------------------
class Ejes{
protected:
    bool esfera;									//true si se quiere una esfera en el origen
    void construir();								//Construye la Display List
public:
    GLuint id;										//Id de la Display List
	Ejes(bool esferaEnOrigen=true):esfera(esferaEnOrigen){construir();}
    void dibujar();				//Dibuja llamando a la DL
};
#endif // RV_AXIS_H
