/***************************************************
Imagenes
Roberto Viv�, 2012 (v1.0)

Carga de im�genes BMP desde fichero.

Dependencias:
glut.h
***************************************************/
#ifndef RV_IMAGES
#define RV_IMAGES

#include <GL/freeglut.h>
#include <freeimage/FreeImage.h>					// Biblioteca de gestion de imagenes

//Lee un BMP y lo carga en tex. Si va bien devuelve 1.
//Devuelve dimensiones del bitmap y formato(GL_RGB)
int leerFicheroBMP(const char* nombre,unsigned int &numCols, 
				   unsigned int &numFilas,GLenum &formato,
				   GLubyte* &tex );


//Lee un BMP de 24 y 8 bpp
int read_image (char * filename, unsigned int *width, unsigned int *height, unsigned char **buffer);



// Carga de imagenes a textura con freeimage (bmp,jpg,png,etc)
void FreeImage_loadTexture(char* nombre);
/*	nombre: nombre del fichero con extensi�n en el mismo directorio que el proyecto
	        o con su path completo
	Uso de FreeImage para cargar la imagen en cualquier formato como textura de 32b
	Incluye la carga de la textura con glTexImage2D()
	Imprescindible inicializar y deinicilizar freeimagen en main

	void main(){
	FreeImage_Initialise();
	...
	glutMainLoop();
	FreeImage_DeInitialise();
	}
*/
#endif